package cl.ex3javiersuarez;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Configures JAX-RS for the application.
 * @author JSuarez
 */
@ApplicationPath("resources")
public class AppConfig extends Application {
    
}