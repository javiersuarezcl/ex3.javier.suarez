/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ex3javiersuarez.services;

import cl.ex3javiersuarez.dao.CervezasJpaController;
import cl.ex3javiersuarez.dao.exceptions.NonexistentEntityException;
import cl.ex3javiersuarez.entity.Cervezas;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author JSuarez
 */
@Path("cervezas")
public class CervezasRest {

    //Crear cerveza
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crear(Cervezas cerveza) {

        CervezasJpaController dao = new CervezasJpaController();
        try {
            dao.create(cerveza);
        } catch (Exception ex) {
            Logger.getLogger(CervezasRest.class.getName()).log(Level.SEVERE, null, ex);
        }

        return Response.ok(200).entity(cerveza).build();
    }

    //Listar cervezas
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarCervezas() {
        CervezasJpaController dao = new CervezasJpaController();
        List<Cervezas> cervezas = dao.findCervezasEntities();

        return Response.ok(200).entity(cervezas).build();
    }

    //Actualilzar cerveza
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizar(Cervezas cerveza) {
        CervezasJpaController dao = new CervezasJpaController();
        try {
            dao.edit(cerveza);
        } catch (Exception ex) {
            Logger.getLogger(CervezasRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(cerveza).build();
    }

    //Eliminar cerveza
    @DELETE
    @Path("{ideliminar}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminar(@PathParam("ideliminar") String ideliminar) {
        CervezasJpaController dao = new CervezasJpaController();
        try {
            dao.destroy(ideliminar);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(CervezasRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("cliente eliminado").entity(ideliminar).build();
    }

    //Consultar cerveza por id
    @GET
    @Path("{idConsulta}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response consultarPorId(@PathParam("idConsulta") String idConsulta) {
        CervezasJpaController dao = new CervezasJpaController();
        Cervezas cerveza = dao.findCervezas(idConsulta);
        return Response.ok(200).entity(cerveza).build();
    }
}
