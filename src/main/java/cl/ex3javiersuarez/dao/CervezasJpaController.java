/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ex3javiersuarez.dao;

import cl.ex3javiersuarez.dao.exceptions.NonexistentEntityException;
import cl.ex3javiersuarez.dao.exceptions.PreexistingEntityException;
import cl.ex3javiersuarez.entity.Cervezas;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author JSuarez
 */
public class CervezasJpaController implements Serializable {

    public CervezasJpaController() {

    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("PU_EX3JAVIERSUAREZ");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Cervezas cervezas) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(cervezas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCervezas(cervezas.getNombre()) != null) {
                throw new PreexistingEntityException("Cervezas " + cervezas + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cervezas cervezas) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            cervezas = em.merge(cervezas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = cervezas.getNombre();
                if (findCervezas(id) == null) {
                    throw new NonexistentEntityException("The cervezas with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cervezas cervezas;
            try {
                cervezas = em.getReference(Cervezas.class, id);
                cervezas.getNombre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cervezas with id " + id + " no longer exists.", enfe);
            }
            em.remove(cervezas);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cervezas> findCervezasEntities() {
        return findCervezasEntities(true, -1, -1);
    }

    public List<Cervezas> findCervezasEntities(int maxResults, int firstResult) {
        return findCervezasEntities(false, maxResults, firstResult);
    }

    private List<Cervezas> findCervezasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Cervezas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cervezas findCervezas(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Cervezas.class, id);
        } finally {
            em.close();
        }
    }

    public int getCervezasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Cervezas> rt = cq.from(Cervezas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
