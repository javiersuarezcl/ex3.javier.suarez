/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.ex3javiersuarez.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Asus
 */
@Entity
@Table(name = "cervezas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cervezas.findAll", query = "SELECT c FROM Cervezas c"),
    @NamedQuery(name = "Cervezas.findByNombre", query = "SELECT c FROM Cervezas c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "Cervezas.findByTipo", query = "SELECT c FROM Cervezas c WHERE c.tipo = :tipo"),
    @NamedQuery(name = "Cervezas.findByGrados", query = "SELECT c FROM Cervezas c WHERE c.grados = :grados"),
    @NamedQuery(name = "Cervezas.findByMililitros", query = "SELECT c FROM Cervezas c WHERE c.mililitros = :mililitros"),
    @NamedQuery(name = "Cervezas.findByContenedor", query = "SELECT c FROM Cervezas c WHERE c.contenedor = :contenedor")})
public class Cervezas implements Serializable {

    @Size(max = 2147483647)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 2147483647)
    @Column(name = "grados")
    private String grados;
    @Size(max = 2147483647)
    @Column(name = "mililitros")
    private String mililitros;
    @Size(max = 2147483647)
    @Column(name = "contenedor")
    private String contenedor;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre")
    private String nombre;

    public Cervezas() {
    }

    public Cervezas(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nombre != null ? nombre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cervezas)) {
            return false;
        }
        Cervezas other = (Cervezas) object;
        if ((this.nombre == null && other.nombre != null) || (this.nombre != null && !this.nombre.equals(other.nombre))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cl.ex3javiersuarez.entity.Cervezas[ nombre=" + nombre + " ]";
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getGrados() {
        return grados;
    }

    public void setGrados(String grados) {
        this.grados = grados;
    }

    public String getMililitros() {
        return mililitros;
    }

    public void setMililitros(String mililitros) {
        this.mililitros = mililitros;
    }

    public String getContenedor() {
        return contenedor;
    }

    public void setContenedor(String contenedor) {
        this.contenedor = contenedor;
    }
    
}
