<%-- 
    Document   : index
    Created on : 17-04-2021, 22:54:05
    Author     : JSuarez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  
        <title> Bienvenido EX 3 </title>
        <style>
            html, body { min-height:97vh; position:relative; margin:0; padding:0; }
            footer { position:absolute; bottom:0px; width:100%; }
            footer p { text-align:center; }
        </style>
    </head>
    <body bgcolor="#87CEFA" text="#3399ff">   
    <center>
        <h1>Bienvenido a mi prueba 3</h1>
        <br>
        <h2>Crear una entrada para una cerveza</h2>
        Creación de cervezas:  - POST - https://ex3javiersuarez.herokuapp.com/resources/cervezas/
       
        <h2>Lista de cervezas</h2>
        Lista de cervezas: - GET - https://ex3javiersuarez.herokuapp.com/resources/cervezas/
        
        <h2>Actualizar la información de una cerveza</h2>
        Actualizacion de cervezas: - PUT - https://ex3javiersuarez.herokuapp.com/resources/cervezas/
        
        <h2>Eliminar una cerveza</h2>
        Eliminación de cervezas: - DELETE - https://ex3javiersuarez.herokuapp.com/resources/cervezas/{ideliminar}/
        
        <h2>Búsqueda de cerveza</h2>
        Traer una de cerveza por su nombre: - GET - https://ex3javiersuarez.herokuapp.com/resources/cervezas/{idConsulta}/
        <!--<table border="0" >
                <tr><td>
                <p><li><a href="CreateController">Registrarse o registrar a un nuevo alumno</a></li></p>
                <p><li><a href="https://ex3javiersuarez.herokuapp.com/resources/comunas/"> Lista de comunas:</a></li></p>
                <p><li><a href="UpdateController"></i>Actualizar los datos</a></li></p>
                <p><li><a href="DeleteController"></i>Borrar su ingreso o de algún alumno</a></li></p>
                </td></tr>-->
        
        <br><br><br><br><br><br><br>
        Javier Alejandro Suárez Guzmán
        <br>
        Sección 7 ó 6
        <br>
        Ramo: TALLER DE APLICACIONES EMPRESARIALES
       <!-- <h1>LOGIN
            <BR>
            INGRESAR:</h1>
        <br><br><br><br><br>
        <h2>Registro</h2>

        <div class="login">    
            <form name="form" id="login" method="POST" action="RegistrarController"> <%-- post método seguro--%>
                <label><b>Nombre de usuario:      
                    </b>    
                </label>    
                <input type="text" name="Uname" id="Uname" placeholder="Nombre de usuario">    
                <br><br>    
                <label><b>Contraseña:      
                    </b>    
                </label>    
                <input type="Password" name="Pass" id="Pass" placeholder="Contraseña secreta">    
                <br><br>    
                <input type="submit" name="log" id="log" value="Ingresar">       
                <br><br>    
                <input type="checkbox" id="check">    
                <span>Recordarme</span>    
                <br><br>    
                Olvido de <a href="#">contraseña</a>    
            </form>     
        </div>    -->
    </body>    
</html>  